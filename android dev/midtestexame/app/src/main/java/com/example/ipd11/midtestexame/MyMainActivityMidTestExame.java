package com.example.ipd11.midtestexame;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    EditText textEdit;
    TextView textView;
    Button showMe ;
    RadioGroup selectedFood;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textEdit = (EditText) findViewById(R.id.editText);
        textView = (TextView) findViewById(R.id.userInfoTextview);
        showMe = (Button) findViewById(R.id.button1);
        selectedFood = (RadioGroup) findViewById(R.id.selectedFood);
    }

    public void btnShow(View view) {

        String name = textEdit.getText().toString();
        String f;
        int food = selectedFood.getCheckedRadioButtonId();

        if(food == R.id.radioButton1)
            f = "Poutine";
        else
            f = "Salmon";


        String toShow = "The user name is " + name + " and the food is " + f;

        textView.setText(toShow);
    }


    public void btnret(View view) {

        String result = "Thank you for using our application " + textEdit.getText().toString();
        Toast.makeText(this,result , Toast.LENGTH_SHORT).show();
    }
}
