package com.example.ipd11.myapptest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Spinner_Entries_Activity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener{

    Spinner mySpinner;
    Button btnAssignColors,btnDisplayTextView, btnAssignDays;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spinner_entries);

        initialize();
    }

    private void initialize() {

        mySpinner = findViewById(R.id.mySpinner);
        mySpinner.setOnItemSelectedListener(this);

        btnAssignColors = findViewById(R.id.btnAssignColors);
        btnAssignColors.setOnClickListener(this);

        btnAssignDays = findViewById(R.id.btnAssignDays);
        btnAssignDays.setOnClickListener(this);

        btnDisplayTextView = findViewById(R.id.btnDisplayTextView);
        btnDisplayTextView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnDisplayTextView:
                //-------------------------------------------- Extract text of selected spinner item to Toast it
                View selectedView = mySpinner.getSelectedView();

                TextView selectedTextView = (TextView) selectedView;
                String selectedText = selectedTextView.getText().toString();
                Toast.makeText(this, "You select : " + selectedText,
                        Toast.LENGTH_SHORT).show();
                break;

            case R.id.btnAssignColors:
                //-------------------------------------------- Assign new array to spinner by adapter
                ArrayAdapter colorAdapter = ArrayAdapter.createFromResource(this,
                        R.array.colors, R.layout.support_simple_spinner_dropdown_item);
                mySpinner.setAdapter(colorAdapter);
                break;

            case R.id.btnAssignDays:
                //-------------------------------------------- Assign new array to spinner by adapter
                ArrayAdapter dayAdapter = ArrayAdapter.createFromResource(this,
                        R.array.days, R.layout.support_simple_spinner_dropdown_item);
                mySpinner.setAdapter(dayAdapter);
                break;
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        TextView textView = (TextView) view;
        String selectTextView = textView.getText().toString();

        Toast.makeText(this,
                "The selected text is " + selectTextView +
                        "\n Position is " + position,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
