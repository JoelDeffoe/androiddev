package com.mysimpledream.sumnumber;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    // Declare the widget
    EditText editText1;
    EditText editText2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Reference the widget
        editText1 = (EditText) findViewById(R.id.editText1);
        editText2 = (EditText) findViewById(R.id.editText2);

    }


    public void doSum(View view){

        // Get the value we typed in widget
        String s1 = editText1.getText().toString();
        String s2 = editText2.getText().toString();

        int i1 = Integer.valueOf(s1);
        int i2 = Integer.valueOf(s2);

        int i3 = i1+i2;

        // Show the typed value
        Toast.makeText(this,String.valueOf(i3), Toast.LENGTH_LONG).show();
    }

}
