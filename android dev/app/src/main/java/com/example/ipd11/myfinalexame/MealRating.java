package com.example.ipd11.myfinalexame;

import android.support.annotation.NonNull;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MealRating implements Comparable {

    private String mealName;
    private int rating;

    public MealRating(String mealName, int rating) {
        this.mealName = mealName;
        this.rating = rating;
    }

    public String getMealName() {
        return mealName;
    }

    public void setMealName(String mealName) {
        this.mealName = mealName;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Meal: " + mealName + "Rating: " + rating;
    }

    @Override
    public int compareTo(@NonNull Object o) {
        MealRating otherObject = (MealRating) o;
        return mealName.compareTo(otherObject.getMealName());
    }
}
