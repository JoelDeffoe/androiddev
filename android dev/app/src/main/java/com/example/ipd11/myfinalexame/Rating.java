package com.example.ipd11.myfinalexame;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;

public class Rating extends AppCompatActivity {

    RadioGroup group;
    RadioButton all;
    RadioButton wrong;
    RadioButton right;
    ImageButton back;
    TextView finalre;
    TextView avrage;
    EditText name;

    String finalResultLable="";
    ArrayList<MealRating> resultSet1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rating);

        initiale();
        Intent intent = getIntent();
        Bundle bundle = intent.getBundleExtra("bundleKey");
        resultSet1 = (ArrayList<MealRating>) bundle.getSerializable("serializableObjectKey");
        String finalRezult = makeResult(resultSet1);
        finalre.setText(finalRezult);
    }

    private void initiale() {

        finalre = findViewById(R.id.result);
        finalre.setMovementMethod(new ScrollingMovementMethod());

        back = findViewById(R.id.btnBack);
        back.setOnClickListener(this);

        name=findViewById(R.id.editText);

        avrage=findViewById(R.id.avrage);

        group=findViewById(R.id.group);

        all=findViewById(R.id.All);

        wrong=findViewById(R.id.Wrong);

        right=findViewById(R.id.Right);
    }

    public String makeResult(ArrayList<MealRating> resultSet1) {
        for (MealRating object : resultSet1) {

            finalResultLable += "Cutomer Order :"+ object.order  + ", Rating : " + object.rate +"\n" ;

        }
        return finalResultLable;
    }

    @Override
    public void onClick(View view) {
        String user=name.getText().toString();
        String setResult="Thank you :" +user;
        Intent intent = new Intent();
        intent.putExtra("Result_ok",setResult);
        setResult(RESULT_OK,intent);
        finish();

    }
    public void test(View view) {
        finalResultLable="";
        clear();
        for (MealRating object : resultSet1) {
            if (object.rate==1) {
                finalResultLable += "Cutomer Order :"+ object.order  + ", Rating : " + object.rate +"\n" ;
            }
        }

        finalre.setText(finalResultLable);

    }

    public void test1(View view) {
        clear();
        finalResultLable="";

        for (MealRating object : resultSet1) {
            if (object.rate==2) {
                finalResultLable += "Cutomer Order :"+ object.order  + ", Rating : " + object.rate +"\n" ;
            }
        }

        finalre.setText(finalResultLable);
    }

    public void clear(){
        finalre.setText("");

    }
    public void test2(View view) {

        finalResultLable="";
        clear();
        for (MealRating object : resultSet1) {
            if (object.rate==3) {
                finalResultLable += "Cutomer Order :"+ object.order  + ", Rating : " + object.rate +"\n" ;
            }
        }

        finalre.setText(finalResultLable);
    }
}
