package com.example.ipd11.myfinalexame;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;

public class FinalExame extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener{

    Spinner spinnerMeal;
    ImageView imageViewMeal;
    RatingBar ratingBarMeal;
    Button btnAdd, btnShowAll, btnSalad, btnMeal;

    String listMeal[] = {"Salmon", "Poutine", "Sushi", "Tacos"};
    int mealPicture[] = {R.drawable.salmon, R.drawable.poutine,
            R.drawable.sushi, R.drawable.tacos};
    String listSalad[] = {"Chicken salad", "Montreal", "Green salad"};
    int saladPicture[] = {R.drawable.salad_a, R.drawable.salad_b,
            R.drawable.salad_c};

    ArrayList<MealRating> listOfMealRating;
    ArrayAdapter<String> mealAdapter;
    ArrayAdapter<String> saladAdapter;

    int tag;





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_final_exame);

        initialize();
    }

    private void initialize() {

        listOfMealRating = new ArrayList<>();

        // Reference to ratingBar.................................
        ratingBarMeal = findViewById(R.id.ratingBar);
        //........................................................


        imageViewMeal = findViewById(R.id.imageView);

        btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(this);

        btnShowAll = findViewById(R.id.btnShowAll);
        btnShowAll.setOnClickListener(this);

        btnMeal = findViewById(R.id.btnMeal);
        btnMeal.setOnClickListener(this);

        btnSalad = findViewById(R.id.btnSalad);
        btnSalad.setOnClickListener(this);

        // Initialize spinner -----------------------------------
        spinnerMeal = findViewById(R.id.spinnerMeal);
        spinnerMeal.setOnItemSelectedListener(this);


        mealAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, listMeal);
        spinnerMeal.setAdapter(mealAdapter);

        saladAdapter = new ArrayAdapter<>(this,
                R.layout.support_simple_spinner_dropdown_item, listSalad);
        spinnerMeal.setAdapter(saladAdapter);
        //-------------------------------------------------------

    }

    // AdapterView.OnItemSelectedListener ----------------------------------------------------------
    @Override
    public void onItemSelected(AdapterView<?> parent, View view,
                               int position, long id) {

        // 'position' is index of selected item in spinner,
        // so we can assign the corresponding image reference
        // from our image array to our imageView

        if (tag==1) {
            int image = mealPicture[position];
            imageViewMeal.setImageResource(image);
        }else {
            int images = saladPicture[position];
            imageViewMeal.setImageResource(images);
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
    //----------------------------------------------------------------------------------------------

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.btnAdd:
                addMealRating();
                break;

            case R.id.btnShowAll:
                showAllMealRating();
                break;

            case  R.id.btnMeal:
                spinnerMeal.setAdapter(mealAdapter);
                tag =1;
                break;

            case R.id.btnSalad:
                spinnerMeal.setAdapter(saladAdapter);
                tag=2;
                break;

        }
    }

    private void addMealRating() {

        // Read ratingBar ....................................
        int rating  = (int) ratingBarMeal.getRating();
        //....................................................

        String meal = spinnerMeal.getSelectedItem().toString();

        // Create new object and add it to our model array....
        MealRating mealRating = new MealRating(meal, rating);
        listOfMealRating.add(mealRating);
        //....................................................

        // Reset rating bar for next time
        ratingBarMeal.setRating(0);
    }

    private void showAllMealRating() {

        Collections.sort(listOfMealRating);

        StringBuilder sb = new StringBuilder("");

        for(MealRating oneMealRating : listOfMealRating) {
            sb.append(oneMealRating + "\n");
        }
        Toast.makeText(this, sb.toString(), Toast.LENGTH_SHORT).show();
    }



}
